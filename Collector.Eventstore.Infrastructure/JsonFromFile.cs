﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Collector.Eventstore.Scavenge
{
    public static class JsonFromFile<T>
    {
        public static T GetObjectFromFile(string file)
        {
            var data = File.ReadAllText(file);
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}
