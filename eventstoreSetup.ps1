param(
[String] $targetPath="C:/tmp/appsettings.json",
[String] $username="eventstorescavenger",
[String] $password="test1234",
[String] $url="localhost:2113")

$sspassword = ($password | ConvertTo-SecureString -AsPlainText -Force) | ConvertFrom-SecureString
dotnet C:/Prodjects/Eventstore/Collector.Eventstore.Setup/bin/debug/netcoreapp2.0/Collector.Eventstore.Setup.dll -t $targetPath -p $sspassword -u $username -url $url