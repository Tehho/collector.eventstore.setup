﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;
using Collector.Eventstore.Domain;
using Collector.Eventstore.Domain.Interfaces;
using Serilog;

namespace Collector.Eventstore.WindowsServicesApp
{
    public partial class MyService : ServiceBase
    {
        private int _eventId;
        private readonly ILogger _logger;
        private readonly IEncoder _passwordEncoder;


        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);

        public MyService(string[] args, ILogger logger, IEncoder passwordEncoder)
        {
            InitializeComponent();

            this.ServiceName = "MyService";
            _eventId = 1;
            _logger = logger;
            _passwordEncoder = passwordEncoder;
        }

        protected override void OnStart(string[] args)
        {
            WriteDebugLog("In OnStart", 0);
               ServiceStatus serviceStatus = new ServiceStatus
            {
                CurrentState = ServiceState.SERVICE_START_PENDING,
                WaitHint = 100000
            };

            SetServiceStatus(ServiceHandle, ref serviceStatus);

            var timer = new System.Timers.Timer
            {
                Interval = 600
            };
            timer.Elapsed += OnTimer;
            timer.Start();


            var scavengerTimer = new Timer
            {
                Interval = new TimeSpan(days: 7, hours: 0, minutes: 0, seconds: 0).TotalMilliseconds
            };
            scavengerTimer.Elapsed += Scavenge;
            scavengerTimer.Start();
        }

        protected override void OnStop()
        {
            WriteDebugLog("In OnStop", 0);

            ServiceStatus serviceStatus = new ServiceStatus
            {
                CurrentState = ServiceState.SERVICE_STOP_PENDING,
                WaitHint = 100000
            };

            SetServiceStatus(ServiceHandle, ref serviceStatus);
        }

        protected void OnTimer(object sender, EventArgs args)
        {
            WriteDebugLog($"Monitoring the System", _eventId++);
        }

        protected void Scavenge(object sender, EventArgs args)
        {
            Task.Run(async () =>
            {
                var key = "";

                var appSettings = new AppSettings();

                appSettings.Password = _passwordEncoder.DecryptString(appSettings.Password, key);

                var scav = new Scavenger(appSettings.Url, appSettings.Username, appSettings.Password);
                var result = await scav.PostToEventStore();

                var resultString = await result.Content.ReadAsStringAsync();
                WriteDebugLog(resultString, 1);
            });
        }

        protected void WriteDebugLog(string message, int eventId)
        {
            _logger.Debug(message);
        }

        protected void WriteInformationLog(string message, int eventId)
        {
            _logger.Information(message);
        }

        protected void WriteErrorLog(string message, int eventId)
        {
            _logger.Error(message);
        }
    }
}
