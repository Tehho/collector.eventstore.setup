﻿using System.Runtime.InteropServices;

namespace Collector.Eventstore.WindowsServicesApp
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ServiceStatus
    {
        public int ServiceType;
        public ServiceState CurrentState;
        public int ControlsAccepted;
        public int Win32ExitCode;
        public int ServiceSpecificExitCode;
        public int CheckPoint;
        public int WaitHint;
    };
}