﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Collector.Eventstore.Domain;
using Serilog;


namespace Collector.Eventstore.WindowsServicesApp
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        private static void Main(string[] args)
        {
            try
            {
                ILogger logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .WriteTo.File("C:/TMP/logfile.txt", rollingInterval: RollingInterval.Day)
                    .CreateLogger();


                var servicesToRun = new ServiceBase[]
                {
                    new MyService(args, logger, new PasswordEncoder())
                };
                ServiceBase.Run(servicesToRun);
            }
            catch (Exception e)
            {
                File.WriteAllText("C:/TMP/logfile.txt", e.Message);
            }
        }
    }
}
