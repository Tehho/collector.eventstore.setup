﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collector.Eventstore.Domain;

namespace Collector.Eventstore.Setup
{
    public class AppSettings
    {
        public string TargetFile { get; set; }
        public ConnectionData ConnectionData { get; set; }

    }
}
