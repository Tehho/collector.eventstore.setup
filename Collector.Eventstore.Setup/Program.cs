﻿using System;
using System.Collections.Generic;
using System.IO;
using Collector.Eventstore.Domain;
using Collector.Eventstore.Domain.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Collector.Eventstore.Setup
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            AppSettings appSettings = SortingArgs(args);
            
            var data = JsonConvert.SerializeObject(appSettings.ConnectionData);

            Console.WriteLine($"Writing to file {appSettings.TargetFile}");
            File.WriteAllText(appSettings.TargetFile, data);

            Console.WriteLine(data);
        }

        private static AppSettings SortingArgs(IReadOnlyList<string> args)
        {
            AppSettings appSettings = new AppSettings
            {
                ConnectionData = new ConnectionData()
                {
                    Password = null,
                    Username = "EventStoreAdmin",
                    Url = "localhost:2113"
                },
                TargetFile = "appsettings.json"
            };

            try
            {
                for (var i = 0; i < args.Count; i++)
                {
                    var arg = args[i].Trim().ToLower();
                    switch (arg)
                    {
                        case "-p":
                        case "-password":
                            appSettings.ConnectionData.Password = args[++i];
                            break;
                        case "-u":
                        case "-username":
                            appSettings.ConnectionData.Username = args[++i];
                            break;
                        case "-url":
                            appSettings.ConnectionData.Url = args[++i];
                            break;
                        case "-t":
                            appSettings.TargetFile = args[++i];
                            break;
                        default:
                            Console.WriteLine($"Command: {arg} not supported");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error in setup.");
                Console.WriteLine(e.Message);
                Console.ResetColor();
            }

            return appSettings;
        }
    }
}

