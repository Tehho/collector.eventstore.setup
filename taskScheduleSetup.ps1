param(
[String] $targetPath,
[String] $taskName)

$task = New-ScheduledTaskAction -Execute "powershell" -Argument $targetPath
$trigger = New-ScheduledTaskTrigger -Weekly -DaysOfWeek Monday -at 1am
Register-ScheduledTask $taskName -Action $task -Trigger $trigger