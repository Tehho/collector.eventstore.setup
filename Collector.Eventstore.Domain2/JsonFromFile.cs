﻿using System.IO;
using Newtonsoft.Json;

namespace Collector.Eventstore.Domain
{
    public static class JsonFromFile<T>
    {
        public static T GetObjectFromFile(string file)
        {
            var data = File.ReadAllText(file);
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}
