﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Collector.Eventstore.Domain.Interfaces;

namespace Collector.Eventstore.Domain
{
    public class PasswordEncoder : IEncoder
    {
        public string EncryptString(string text, string keyString)
        {
            byte[] key;

            try
            {
                if (keyString.Length % 2 == 1)
                    throw new ArgumentOutOfRangeException(nameof(keyString), "KeyString can not have an do number of hexes");
                key = FromStringToBytes(keyString)
                    .Take(32).ToArray();
            }
            catch (Exception)
            {
                key = Encoding.UTF8.GetBytes(keyString);
            }


            using (var aesAlg = Aes.Create())
            using (var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV))
            using (var msEncrypt = new MemoryStream())
            {
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                using (var swEncrypt = new StreamWriter(csEncrypt))
                {
                    swEncrypt.Write(text);
                }

                var iv = aesAlg.IV;

                var decryptedContent = msEncrypt.ToArray();

                var result = new byte[iv.Length + decryptedContent.Length];

                Buffer.BlockCopy(iv, 0, result, 0, iv.Length);
                Buffer.BlockCopy(decryptedContent, 0, result, iv.Length, decryptedContent.Length);

                return Convert.ToBase64String(result);
            }
        }

        public string DecryptString(string cipherText, string keyString)
        {
            var fullCipher = Convert.FromBase64String(cipherText);

            var iv = new byte[16];
            var cipher = new byte[fullCipher.Length - iv.Length];

            Buffer.BlockCopy(fullCipher, 0, iv, 0, iv.Length);
            Buffer.BlockCopy(fullCipher, iv.Length, cipher, 0, cipher.Length);

            var key = Encoding.UTF8.GetBytes(keyString);

            using (var aesAlg = Aes.Create())
            using (var decryptor = aesAlg.CreateDecryptor(key, iv))
            {
                string result;

                using (var msDecrypt = new MemoryStream(cipher))
                using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                using (var srDecrypt = new StreamReader(csDecrypt))
                {
                    result = srDecrypt.ReadToEnd();
                }

                return result;
            }
        }

        private byte[] FromStringToBytes(string hexString)
        {
            if (Regex.IsMatch(hexString, "^[^0-9a-fA-F]$"))
                throw new ArgumentException("Input not all hex values", nameof(hexString));

            var hexAsBytes = new byte[hexString.Length >> 1];

            for (var i = 0; i < hexAsBytes.Length; i++)
            {
                hexAsBytes[i] = (byte) (
                    (GetHexValue(hexString[i << 1]) << 4) +
                    (GetHexValue(hexString[(i << 1) + 1])));
            }

            return hexAsBytes;
        }

        private int GetHexValue(char c)
        {
            return c - (c <= '9' ? '0' : c <= 'Z' ? ('A' - 10) : ('a' - 10));
        }
    }
}