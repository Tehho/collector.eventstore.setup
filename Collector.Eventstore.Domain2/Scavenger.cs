﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Collector.Eventstore.Domain.Interfaces;

namespace Collector.Eventstore.Domain
{
    public class Scavenger : IScavenger
    {
        private readonly HttpClient _client;
        private readonly ConnectionData _connectionData;

        public Scavenger(ConnectionData connectionData)
        {
            _connectionData = connectionData;
        }
        public Scavenger(string url, string username, string password)
        {
            _client = new HttpClient();
            _connectionData = new ConnectionData()
            {
                Url = url,
                Username = username,
                Password = password
            };
        }

        public async Task<HttpResponseMessage> PostToEventStore()
        {
            try
            {
                var content = new FormUrlEncodedContent(new Dictionary<string, string>());

                return await _client.PostAsync(GenerateConnectionUrl(), content);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private string GenerateConnectionUrl()
        {
            return $"https://{_connectionData.Username}:{_connectionData.Password}@{_connectionData.Url}/admin/scavenge";
        }
    }
}