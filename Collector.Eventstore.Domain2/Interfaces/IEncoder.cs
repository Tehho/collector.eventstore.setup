﻿namespace Collector.Eventstore.Domain.Interfaces
{
    public interface IEncoder
    {
        string EncryptString(string text, string keyString);
        string DecryptString(string cipherText, string keyString);
    }
}