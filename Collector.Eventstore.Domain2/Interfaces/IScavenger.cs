﻿using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Collector.Eventstore.Domain.Interfaces
{
    public interface IScavenger
    {
        Task<HttpResponseMessage> PostToEventStore();
    }
}
