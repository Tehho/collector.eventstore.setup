﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collector.Eventstore.Domain
{
    public class ConnectionData
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Url { get; set; }
    }
}
