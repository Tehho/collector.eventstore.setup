﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Collector.Eventstore.Domain;
using Collector.Eventstore.Scavenge;
using Newtonsoft.Json;
using Serilog;
using Serilog.Sinks.Elasticsearch;

namespace Collector.Eventstore.Scavenge
{
    internal class Program
    {
        private static ILogger _logger;
        private static async Task Main(string[] args)
        {
            
            var uri = new Uri("http://localhost:9200/");

            var elasticsearchOptions = new ElasticsearchSinkOptions(uri)
            {
                AutoRegisterTemplate = true,
                AutoRegisterTemplateVersion = AutoRegisterTemplateVersion.ESv6,
            };


            _logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                .WriteTo.Elasticsearch(elasticsearchOptions)
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .CreateLogger();

            _logger.Debug("Sorting Arguments");
            var appSettings = SortingArgs(args);

            try
            {
                _logger.Information("Starting scavenge");
                var app = new App(appSettings.ConnectionData, _logger);
                await app.Run();
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }

            Console.ReadLine();
        }

        private static AppSettings SortingArgs(IReadOnlyList<string> args)
        {
            var appSettings = new AppSettings();
            try
            {
                for (var i = 0; i < args.Count; i++)
                {
                    var arg = args[i];
                    switch (arg)
                    {
                        case "-p":
                        case "-password":
                            appSettings.ConnectionData.Password = args[++i];
                            _logger.Debug("Password found");
                            break;
                        case "-u":
                        case "-username":
                            appSettings.ConnectionData.Username = args[++i];
                            _logger.Debug("Username found");
                            break;
                        case "-url":
                            appSettings.ConnectionData.Url = args[++i];
                            _logger.Debug("Url found");
                            break;
                        default:
                            _logger.Debug("Found argument that does not exist {arg}", arg);
                            break;
                    }
                }
            }
            catch (Exception)
            {
                _logger.Error("Error sorting inparameters in Scavenge");
            }

            return appSettings;
        }
    }
}
