﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collector.Eventstore.Domain;

namespace Collector.Eventstore.Scavenge
{
    public class AppSettings
    {
        public ConnectionData ConnectionData { get; set; }

        public AppSettings()
        {
            ConnectionData = new ConnectionData();
        }
    }
}
