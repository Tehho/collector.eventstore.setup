﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Collector.Eventstore.Domain;
using Collector.Eventstore.Domain.Interfaces;
using Serilog;

namespace Collector.Eventstore.Scavenge
{
    public class App
    {
        private readonly IScavenger _scavenger;
        private readonly ILogger _logger;

        public App(ConnectionData connectionData, ILogger logger)
        {
            _scavenger = new Scavenger(connectionData.Url, connectionData.Username, connectionData.Password);
            _logger = logger;
        }

        public App(string url, string username, string password, ILogger logger)
        {
            _scavenger = new Scavenger(url, username, password);
            _logger = logger;
        }

        public async Task Run()
        {
            _logger.Debug("Posting to scavange");
            var response = await _scavenger.PostToEventStore(); ;

            _logger.Debug("Logging response");
            await LogResponse(response);
        }

        private async Task LogResponse(HttpResponseMessage response)
        {
            if (response == null)
            {
                _logger.Error("Scavenge failed to post");
            }
            else if (response.StatusCode != HttpStatusCode.OK)
            {
                _logger.Error("Scavenge failed to post");
            }
            else
            {
                _logger.Information("Scavenge posted");
            }
        }
    }
}
