param(
[String] $targetPath,
[String] $appsettingsPath)

$data = Get-Content -path $appsettingsPath
$appsettings = $data | ConvertFrom-Json

$sspassword = ConvertTo-SecureString -String $appsettings.password
$password = [System.Net.NetworkCredential]::new('', $sspassword).password
$username = $appsettings.username
$url = $appsettings.url

dotnet $targetPath -p $password -u $username -url $url